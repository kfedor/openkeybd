#ifndef _FFTDI
#define _FFTDI

#include <stdio.h>
#include <string.h>
#include <ftdi.h>
#include <unistd.h>
#include <syslog.h>

#define VID 0x0403
#define PID 0x6001
#define SERIAL "A501H07X"
#define SPEED 115200
#define REPLEN 12

class FFtdi
{
	public:
		FFtdi();
		~FFtdi();

		bool openDev();
		void stop();
		bool sendCmd( unsigned char *data, unsigned int len, unsigned char *reply, unsigned int *reply_size );
		bool isConnected() { return connected; }

	private:		
		bool connected = false;
        bool error_msg = false;
		struct ftdi_context ftdic;
		bool init();
};

#endif
