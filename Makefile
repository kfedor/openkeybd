SRC = $(wildcard *.cpp)
OUT = openkeybd
LIBS = -lftdi -lpthread /usr/lib/x86_64-linux-gnu/libjsoncpp.a
INC = -I/usr/include/jsoncpp

all:
	c++ -std=c++11 -O2 -Wall $(INC) $(SRC) $(LIBS) -o $(OUT)
	strip $(OUT)
