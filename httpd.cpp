#include "httpd.h"
#include "help.h"

HTTPD::HTTPD()
{
	sockfd = 0;
	global_lights = 0;
}

HTTPD::~HTTPD()
{
	if( sockfd > 0 )
		shutdown( sockfd, SHUT_RDWR );

	syslog( LOG_INFO, "HTTP server stopped" );
}

bool HTTPD::init( int portno )
{
	struct sockaddr_in serv_addr;

	/* First call to socket() function */
	sockfd = socket( AF_INET, SOCK_STREAM, 0 );

	if( sockfd < 0 )
	{
		syslog( LOG_INFO, "Error opening socket" );
		return false;
	}

	/* Initialize socket structure */
	memset( ( char * ) &serv_addr, 0, sizeof( serv_addr ) );

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons( portno );

	/* Now bind the host address using bind() call.*/
	if( bind( sockfd, ( struct sockaddr * ) &serv_addr, sizeof( serv_addr ) ) < 0 )
	{
		syslog( LOG_INFO, "Error binding socket (port probably in use)" );
		return false;
	}

	listen( sockfd, 20 );

	syslog( LOG_INFO, "HTTP server started on port %d", portno );

	ufds.fd = sockfd;
	ufds.events = POLLIN; // check for just normal data

	return klava.runThread();
}

bool HTTPD::processConnections()
{
	int newsockfd = -1;

	try
	{
		char buffer[1024] = {0};

		struct sockaddr_in cli_addr;
		int clilen = sizeof( cli_addr );

		if( poll( &ufds, 1, 3500 ) <= 0 )
		{
			return false;
		}

		newsockfd = accept( sockfd, ( struct sockaddr * )&cli_addr, ( socklen_t* )&clilen );

		if( newsockfd < 0 )
			throw std::runtime_error( "Unable to accept new connection" );

		/* If connection is established then start communicating */
		int n = read( newsockfd, buffer, sizeof( buffer )-1 );

		if( n <= 0 )
			throw std::runtime_error( "Read error" );

		//uint64_t reqTime = klava.getTimeMs();
		//syslog( LOG_INFO, "Request time: %ld", reqTime );

		char *first = strchr( buffer, 0x20 );
		char *second = strchr( first+1, 0x20 );

		if( first == NULL || second == NULL )
			throw std::runtime_error( "Header parsing error" );

		int url_len = second - first - 1;

		char uri[255]= {0};
		strncpy( uri, first+1, url_len );

		writeCommonHeader( newsockfd );

		if( strncmp( uri, "/keys.cgi", 9 ) == 0 )
		{
			char *begin = strchr( uri, '=' );

			if( begin )
			{
				global_lights = atoi( begin + 1 );
				klava.setLights( global_lights );
			}

			writeKeys( newsockfd );
		}
		else if( strncmp( uri, "/lights.cgi", 11 ) == 0 )
		{
			char *begin = strchr( uri, '=' );

			if( begin )
			{
				global_lights = atoi( begin + 1 );
				klava.setLights( global_lights );
			}

			writeLights( newsockfd );
		}
		else if( strncmp( uri, "/keycodes.cgi", 13 ) == 0 )
		{
			writeKeycodes( newsockfd );
		}
		else
		{
			writeHelp( newsockfd );
		}
	}

	catch( std::exception &ex )
	{
		syslog( LOG_INFO, "HTTPD connection error [%s]", ex.what() );

		if( newsockfd > 0 )
			close( newsockfd );

		return false;
	}

	close( newsockfd );
	return true;
}

void HTTPD::writeData( int sockfd, std::string data )
{
//	int n = write( sockfd, data.c_str(), data.length() );
	int n = send( sockfd, data.c_str(), data.length(), MSG_NOSIGNAL );

	if( n < 0 )
		throw std::runtime_error( "Error writing to socket" );
}

void HTTPD::writeCommonHeader( int newsockfd )
{
	writeData( newsockfd, "HTTP/1.1 200 OK\r\n" );
	writeData( newsockfd, "Server: OpenKbd\r\n" );
	writeData( newsockfd, "Cache-Control: no-store, no-cache, must-revalidate\r\n" );
	writeData( newsockfd, "Pragma: no-cache\r\n" );
	writeData( newsockfd, "Connection: close\r\n" );
	writeData( newsockfd, "Access-Control-Allow-Origin: *\r\n" );
}

void HTTPD::writeHelp( int newsockfd )
{
	std::ostringstream oss;
	oss << "Content-Length: " << helpstring.length() << "\r\n";

	writeData( newsockfd, "Content-Type: text/html; charset=utf-8\r\n" );
	writeData( newsockfd, oss.str() );
	writeData( newsockfd, "\r\n" );
	writeData( newsockfd, helpstring );
}

void HTTPD::writeKeycodes( int newsockfd )
{
	std::ostringstream oss;
	oss << "Content-Length: " << keyhelp.length() << "\r\n";

	writeData( newsockfd, "Content-Type: application/json\r\n" );
	writeData( newsockfd, oss.str() );
	writeData( newsockfd, "\r\n" );
	writeData( newsockfd, keyhelp );
}

void HTTPD::writeKeys( int newsockfd )
{
	Json::Value out;
	Json::StyledWriter writer;
	std::deque<Klava::kbd_events> events = klava.getEvents();

	out["timeNow"] = ( Json::Value::UInt64 )klava.getTimeMs(); //( int )time( nullptr );
	out["lights"] = global_lights;
	out["keys"] = klava.getKeys();
	klava.clearKeys();

	for( auto i: events )
	{
		Json::Value je;
		je["pressed"] = i.pressed;
		je["released"] = i.released;
		je["time"] = ( Json::Value::UInt64 )i.time;
		out["events"].append( je );
	}

	std::string out_json = writer.write( out );

	std::ostringstream oss;
	oss << "Content-Length: " << out_json.length() << "\r\n";

	writeData( newsockfd, "Content-Type: application/json\r\n" );
	writeData( newsockfd, oss.str() );
	writeData( newsockfd, "\r\n" );
	writeData( newsockfd, out_json );

	//syslog( LOG_INFO, "%s", out_json.c_str() );
}

void HTTPD::writeLights( int newsockfd )
{
	Json::Value out;
	Json::FastWriter writer;

	out["timeNow"] = ( Json::Value::UInt64 )klava.getTimeMs(); //( int )time( nullptr );
	out["lights"] = global_lights;

	std::string out_json = writer.write( out );

	std::ostringstream oss;
	oss << "Content-Length: " << out_json.length() << "\r\n";

	writeData( newsockfd, "Content-Type: application/json\r\n" );
	writeData( newsockfd, oss.str() );
	writeData( newsockfd, "\r\n" );
	writeData( newsockfd, out_json );
}
