#include "fftdi.h"

FFtdi::FFtdi()
{
	init();
}

FFtdi::~FFtdi()
{
	stop();
}

bool FFtdi::init()
{
	if( ftdi_init( &ftdic ) < 0 )
	{
		syslog( LOG_INFO, "Error initializing FTDI" );
		return false;
	}

	ftdi_set_interface( &ftdic, INTERFACE_A );

	return true;
}

bool FFtdi::openDev()
{
	if( ftdi_usb_open_desc( &ftdic, VID, PID, NULL, NULL ) < 0 )
	{
		if( error_msg == false ) 
        {
            syslog( LOG_INFO, "Error opening FTDI device" );
            error_msg = true;        
        }
        
		return false;
	}

	ftdi_set_baudrate( &ftdic, SPEED );
	connected = true;
	
	syslog( LOG_INFO, "Device connected" );

	return true;
}

void FFtdi::stop()
{
	if( connected )
		ftdi_usb_close( &ftdic );

	ftdi_deinit( &ftdic );
}

bool FFtdi::sendCmd( unsigned char *data, unsigned int len, unsigned char *reply, unsigned int *reply_size )
{
	unsigned int numread = 0;

	if( ftdi_write_data( &ftdic, data, len ) < 0 )
	{
		syslog( LOG_INFO, "FTDI write error" );
		connected = false;
		return false;
	}

	for( int i=0; i<100; i++ )
	{
		unsigned char tmp = 0;
		int bread = ftdi_read_data( &ftdic, ( unsigned char* )&tmp, 1 );

		if( bread < 0 )
		{
			syslog( LOG_INFO, "FTDI read error" );
			connected = false;
			return false;
		}

		if( bread )
		{
			reply[numread] = tmp;
			numread += bread;

			if( numread == REPLEN )
				break;
		}
		else
		{
			usleep( 100 );
		}
	}

	*reply_size = numread;

	return true;
}
