#ifndef _HTTPD
#define _HTTPD

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string>
#include <sstream>
#include <exception>
#include <stdexcept>
#include <vector>
#include <syslog.h>
#include <sys/poll.h>

#include <json/reader.h>
#include <json/writer.h>
#include <json/value.h>

#include "klava.h"

class HTTPD
{
	public:
		HTTPD();
		~HTTPD();
		bool init( int portno );
		bool processConnections();

	private:
		int sockfd;
		int newsockfd;
		struct pollfd ufds;
		
		Klava klava;

		void writeData( int, std::string );
		void writeCommonHeader( int newsockfd );
		void writeHelp( int newsockfd );
		void writeKeycodes( int newsockfd );
		void writeKeys( int newsockfd );
		void writeLights( int newsockfd );

		unsigned short global_lights;
};

#endif
