#include <signal.h>
#include <syslog.h>
#include "httpd.h"

bool stop = false;

static void signal_handler( int signo )
{
	syslog( LOG_INFO, "Signal handler [%d]", signo );
	stop = true;
}

int main()
{
	openlog( "openkeybd", LOG_CONS | LOG_NDELAY | LOG_PERROR | LOG_PID, LOG_USER );
	setlogmask( LOG_UPTO( LOG_DEBUG ) );

	HTTPD httpd;

	signal( SIGINT, signal_handler );
	signal( SIGTERM, signal_handler );	

	if( !httpd.init( 80 ) )
		return 0;

	while( !stop )
		httpd.processConnections();

	syslog( LOG_INFO, "Exiting" );
	closelog();

	return 0;
}
