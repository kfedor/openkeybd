#ifndef _KLAVA
#define _KLAVA

#include <thread>
#include <exception>
#include <atomic>
#include <deque>
#include <mutex>
#include <bitset>
#include <sys/time.h>
#include <syslog.h>

#include "fftdi.h"
#include "crc.h"

#define LOBYTE(w) ((char)((short)(w) & 0xff))
#define HIBYTE(w) ((char)((short)(w) >> 8))

#define EVENTS_SIZE 50

class Klava
{
	public:
		Klava();
		~Klava();

		struct kbd_events
		{
			unsigned short pressed;
			unsigned short released;
			uint64_t time;
		};

		bool runThread();
		void setLights( unsigned short li ) { atom_lights = li; }		
		unsigned short getKeys() { return atom_keys; }
		void clearKeys() { atom_keys = 0; }
		std::deque<Klava::kbd_events> getEvents();
		uint64_t getTimeMs();

	private:
		std::atomic<unsigned short> atom_lights;
		std::atomic<unsigned short> atom_keys;
		std::atomic<bool> atom_stop;		

		std::thread handler;
		std::deque<Klava::kbd_events> events;
		std::mutex ev_lock;
		kbd_events prev_event;

		FFtdi fftdi;
		CRC crc;

		void addKeysEvent( unsigned short keys );
		static void threadHandler( Klava *ptr );
		short poll( unsigned short light );
		//short setUpperByte( short, unsigned char );
		//short setLowerByte( short, unsigned char );
		short ftdiLights( unsigned short a1 );
		short gameKeysToKeys( unsigned short a1 );

		void printHex( unsigned char *cmd, int size );
};

#endif
