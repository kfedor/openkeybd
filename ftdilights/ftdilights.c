#define LOBYTE(w) ((char)((short)(w) & 0xff))
#define HIBYTE(w) ((char)((short)(w) >> 8))

short setUpperByte( short number, unsigned char upper )
{
	short result = ( number & 0x00ff ) | ( upper << 8 );
	return result;
}

short setLowerByte( short number, unsigned char lower )
{
	short result = ( number & 0xff00 ) | lower;
	return result;
}

/*
short cvtGameKeysToGameLights( unsigned short a1 )
{
	short result = 0; // ax@1
	short v2 = 0; // cx@1
	short v3 = 0; // cx@11
	short v4 = 0; // cx@13
	short v5 = 0; // cx@19

	result = -( a1 & 1 ) & 0x400;
	v2 = -( a1 & 1 ) & 0x400;

	// HIBYTE( v2 ) |= 2u;
	// HIBYTE( v2 ) = HIBYTE(v2) | 2u;
	v2 = setUpperByte( v2, HIBYTE( v2 ) | 2u );	

	if( a1 & 2 )
		result = v2;

	if( a1 & 4 )
		result |= 8u;

	if( a1 & 8 )
		result |= 0x10u;

	if( a1 & 0x10 )
		result |= 0x20u;

	if( a1 & 0x20 )
		result |= 0x40u;

//	HIBYTE( v3 ) = HIBYTE( result );
	v3 = setUpperByte( v3, HIBYTE( result ) );

	if( a1 & 0x40 )
	{
		//LOBYTE( v3 ) = result | 0x80;
		v3 = setLowerByte( v3, result|0x80 );
		result = v3;
	}

	//LOBYTE( v4 ) = result;
	v4 = setLowerByte( v4, result );

	if( a1 & 0x80 )
	{
		//HIBYTE( v4 ) = HIBYTE( result ) | 1;
		v4 = setUpperByte( v4, HIBYTE( result ) | 1 );
		result = v4;
	}

	if( HIBYTE( a1 ) & 1 )
		result |= 2u;

	if( HIBYTE( a1 ) & 2 )
		result |= 4u;

	//LOBYTE( v5 ) = result;
	v5 = setLowerByte( v5, result );

	if( HIBYTE( a1 ) & 4 )
	{
		//HIBYTE( v5 ) = HIBYTE( result ) | 8;
		v5 = setUpperByte( v5, HIBYTE( result ) | 8 );
		result = v5;
	}

	if( HIBYTE( a1 ) & 8 )
		result |= 1u;

	return result;
}
*/

// Число в команду для контроллера
short ftdiLights( unsigned short a1 )
{
	short result = 0;
	short v2 = 0;
	short v3 = 0;
	short v4 = 0;
	short v5 = 0;
	
	result = -( a1 & 1 ) & 0x400;

	if( a1 & 2 )
		result |= 0x10u;

	if( a1 & 4 )
		result |= 0x20u;

	//LOBYTE( v2 ) = result;
	v2 = setLowerByte( v2, result );	

	if( a1 & 8 )
	{
		//HIBYTE( v2 ) = HIBYTE( result ) | 1;
		v2 = setUpperByte( v2, HIBYTE( result ) | 1 );		
		result = v2;		
	}

	if( a1 & 0x10 )
	{
		result |= 8u;		
	}

	if( a1 & 0x20 )
	{
		result |= 4u;		
	}

	if( a1 & 0x40 )
	{
		result |= 2u;		
	}

	if( a1 & 0x80 )
	{
		result |= 1u;		
	}

	//HIBYTE( v3 ) = HIBYTE( result );
	v3 = setUpperByte( v3, HIBYTE( result ) );	

	if( HIBYTE( a1 ) & 1 )
	{
		//LOBYTE( v3 ) = result | 0x80;
		v3 = setLowerByte( v3, result | 0x80 );		
		result = v3;			
	}

	if( HIBYTE( a1 ) & 2 )
	{
		result |= 0x40u;			
	}

	//LOBYTE( v4 ) = result;
	v4 = setLowerByte( v4, result );	

	if( HIBYTE( a1 ) & 4 )
	{
		//HIBYTE( v4 ) = HIBYTE( result ) | 2;
		v4 = setUpperByte( v4, HIBYTE( result ) | 2 );		
		result = v4;
	}

	//LOBYTE( v5 ) = result;
	v5 = setLowerByte( v5, result );	

	if( HIBYTE( a1 ) & 8 )
	{
		//HIBYTE( v5 ) = HIBYTE( result ) | 8;
		v5 = setUpperByte( v5, HIBYTE( result ) | 8 );		
		result = v5;
	}

	return result;
}

int main()
{
	printf( "result %04X\n", ftdiLights( 1 ) );
	printf( "result %04X\n", ftdiLights( 2 ) );
	printf( "result %04X\n", ftdiLights( 3 ) );
	printf( "result %04X\n", ftdiLights( 4 ) );
	printf( "result %04X\n", ftdiLights( 8 ) );
	printf( "result %04X\n", ftdiLights( 16 ) );
	printf( "result %04X\n", ftdiLights( 32 ) );
	printf( "result %04X\n", ftdiLights( 64 ) );
	printf( "result %04X\n", ftdiLights( 128 ) );
	printf( "result %04X\n", ftdiLights( 256 ) );
	printf( "result %04X\n", ftdiLights( 512 ) );
	printf( "result %04X\n", ftdiLights( 1024 ) );
	printf( "result %04X\n", ftdiLights( 2048 ) );	
	
	/*
	printf( "result %04X\n", cvtGameKeysToGameLights( 0x0001 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 0x0002 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 0x0410 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 0x0020 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 0x0100 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 0x0008 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 0x0004 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 0x0002 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 0x0001 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 0x0080 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 0x0040 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 0x0200 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 0x0800 ) );
	*/
	
	/*
	printf( "result %04X\n", cvtGameKeysToGameLights( 1 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 2 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 3 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 4 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 8 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 16 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 32 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 64 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 128 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 256 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 512 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 1024 ) );
	printf( "result %04X\n", cvtGameKeysToGameLights( 2048 ) );
	*/
	
	return 0;
}
