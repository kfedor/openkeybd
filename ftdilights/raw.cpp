#include <stdio.h>


template<typename T>
inline T set_hibyte(T val, unsigned char cb){
   const int n = (sizeof(T) << 3) - 8; 
   return (val & ~(0xFF << n)) | ((T)cb << n);
}


int  main(void) {
   unsigned short val = 0x1234; // word
   printf("src: %X\n", val);
   printf("dst: %X\n", set_hibyte(val, 0xEF));

   unsigned int val1 = 0x12345678; // dword
   printf("src: %X\n", val1);
   printf("dst: %X\n", set_hibyte(val1, 0xAB));
   return 0;
}