#include "klava.h"

uint64_t Klava::getTimeMs()
{
	struct timeval te;
	gettimeofday( &te, NULL ); // get current time
	uint64_t milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // caculate milliseconds
	return milliseconds;
	/*
	struct timespec tp;
	clock_gettime( 0, &tp );
	return 10000000LL * ( signed int )tp.tv_sec + ( signed int )tp.tv_nsec / 0x64uLL;
	*/
}

Klava::Klava()
{
	atom_stop = false;
	atom_lights = 0;
	atom_keys = 0;
	memset( &prev_event, 0, sizeof( kbd_events ) );
}

Klava::~Klava()
{
	if( handler.joinable() )
	{
		atom_stop = true;
		handler.join();
	}
}

bool Klava::runThread()
{
	try
	{
		handler = std::thread( threadHandler, this );
	}

	catch( const std::exception &ex )
	{
		syslog( LOG_INFO, "Error creating thread" );
		return false;
	}

	return true;
}

void Klava::addKeysEvent( unsigned short keys )
{
	/*
	kbd_events prev_event;

	if( events.size() > 0 )
		prev_event = events.back();
	else
		memset( &prev_event, 0, sizeof( kbd_events ) );
	*/
	if( prev_event.pressed == keys ) // no new keys
		return;

	syslog( LOG_INFO, "[%ld] Keys: %d", getTimeMs(), keys );

	// All pressed keys
	atom_keys |= keys;

	kbd_events tmp;
	tmp.pressed = keys;
	tmp.released = ( keys ^ prev_event.pressed ) & prev_event.pressed;
	tmp.time = getTimeMs();

	memcpy( &prev_event, &tmp, sizeof( kbd_events ) );

	std::lock_guard<std::mutex> lock( ev_lock );
	events.push_back( tmp );

	// Limit check
	if( events.size() > EVENTS_SIZE )
		events.pop_front(); // remove oldest event
}

std::deque<Klava::kbd_events> Klava::getEvents()
{
	std::lock_guard<std::mutex> lock( ev_lock );
	std::deque<Klava::kbd_events> tmp = std::move( events );
	return tmp;
}

void Klava::threadHandler( Klava *ptr )
{
	while( !ptr->atom_stop )
	{
		if( ptr->fftdi.isConnected() )
		{
			short tmp_keys = ptr->poll( ptr->atom_lights );

			if( tmp_keys >= 0 )
				ptr->addKeysEvent( tmp_keys );

			usleep( 50000 ); // 50000 = 50ms
		}
		else
		{
			ptr->fftdi.openDev();
			sleep( 1 );
		}
	}
}

short Klava::poll( unsigned short light )
{
	unsigned char reply[REPLEN] = {0};
	unsigned int reply_size;

	//unsigned char cmd[] = { 0x55, 0xAA, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x84 };
	unsigned char cmd[] = { 0x55, 0xAA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

	unsigned short ftdi_light = ftdiLights( light );

	cmd[2] = LOBYTE( ftdi_light );
	cmd[3] = HIBYTE( ftdi_light );

	unsigned short new_crc = crc.getCrcCcitt( ( char* )&cmd+2, sizeof( cmd )-4 );

	cmd[6] = LOBYTE( new_crc );
	cmd[7] = HIBYTE( new_crc );

	//printf( "request: " );
	//printHex( cmd, sizeof( cmd ) );

	if( !fftdi.sendCmd( ( unsigned char* )&cmd, sizeof( cmd ), ( unsigned char* )&reply, &reply_size ) )
	{
		syslog( LOG_INFO, "FTDI device unavailable" );
		return -1;
	}

	//printf( "reply: " );
	//printHex( reply, reply_size );

	if( reply[0] != 0x55 && reply[1] != 0xAA )
		return -1;

	unsigned short keys = ( unsigned short )( ( ( unsigned char )reply[9] ) << 8 | ( ( unsigned char )reply[8] ) );

	return gameKeysToKeys( keys );
}
/*
short Klava::setUpperByte( short number, unsigned char upper )
{
	short result = ( number & 0x00ff ) | ( upper << 8 );
	return result;
}

short Klava::setLowerByte( short number, unsigned char lower )
{
	short result = ( number & 0xff00 ) | lower;
	return result;
}
*/
/*
short Klava::ftdiLights( unsigned short a1 )
{
	short result = 0;
	short v2 = 0;
	short v3 = 0;
	short v4 = 0;
	short v5 = 0;

	result = -( a1 & 1 ) & 0x400;

	if( a1 & 2 )
		result |= 0x10u;

	if( a1 & 4 )
		result |= 0x20u;

	v2 = setLowerByte( v2, result );

	if( a1 & 8 )
	{
		v2 = setUpperByte( v2, HIBYTE( result ) | 1 );
		result = v2;
	}

	if( a1 & 0x10 )
	{
		result |= 8u;
	}

	if( a1 & 0x20 )
	{
		result |= 4u;
	}

	if( a1 & 0x40 )
	{
		result |= 2u;
	}

	if( a1 & 0x80 )
	{
		result |= 1u;
	}

	v3 = setUpperByte( v3, HIBYTE( result ) );

	if( HIBYTE( a1 ) & 1 )
	{
		v3 = setLowerByte( v3, result | 0x80 );
		result = v3;
	}

	if( HIBYTE( a1 ) & 2 )
	{
		result |= 0x40u;
	}

	v4 = setLowerByte( v4, result );

	if( HIBYTE( a1 ) & 4 )
	{
		v4 = setUpperByte( v4, HIBYTE( result ) | 2 );
		result = v4;
	}

	v5 = setLowerByte( v5, result );

	if( HIBYTE( a1 ) & 8 )
	{
		v5 = setUpperByte( v5, HIBYTE( result ) | 8 );
		result = v5;
	}

	return result;
}
*/
short Klava::ftdiLights( unsigned short in )
{
	std::bitset<12> bin = in;
	std::bitset<12> bout = 0;

	if( bin.test( 0 ) )  //
		bout.set( 10 );   // 1 pin = 1024

	if( bin.test( 1 ) )  //
		bout.set( 4 );  // 2 pin = 16

	if( bin.test( 2 ) )  //
		bout.set( 5 );  // 3 pin = 32

	if( bin.test( 3 ) )  //
		bout.set( 8 );   // 4 pin = 256

//----
	if( bin.test( 4 ) ) //
		bout.set( 3 );   // 5 pin = 8

	if( bin.test( 5 ) ) //
		bout.set( 2 );   // 6 pin = 4

	if( bin.test( 6 ) ) //
		bout.set( 1 );   // 7 pin = 2

	if( bin.test( 7 ) ) //
		bout.set( 0 );   // 8 pin = 1

//----
	if( bin.test( 8 ) )  //
		bout.set( 7 );   // 9 pin = 128

	if( bin.test( 9 ) )  //
		bout.set( 6 );   // 10 pin = 64

	if( bin.test( 10 ) ) //
		bout.set( 9 );   // 11 pin = 512

	if( bin.test( 11 ) ) //
		bout.set( 11 );	 // 12 = 2048

	return ( unsigned short )bout.to_ulong();
}

short Klava::gameKeysToKeys( unsigned short in )
{
	std::bitset<16> bin = in;
	std::bitset<12> bout = 0;

	if( bin.test( 0 ) )  // 1 pin = 1
		bout.set( 9 );  // 1 pin = 512

	if( bin.test( 1 ) )  // 2 pin = 2
		bout.set( 2 );   // 2 pin = 4

	if( bin.test( 2 ) )  // 3 pin = 4
		bout.set( 1 );   // 3 pin = 2

	if( bin.test( 3 ) )  // 4 pin = 8
		bout.set( 7 );   // 4 pin = 128

//----
	if( bin.test( 15 ) ) // 5 pin = 32768
		bout.set( 6 );   // 5 pin = 64

	if( bin.test( 14 ) ) // 6 pin = 16384
		bout.set( 5 );   // 6 pin = 32

	if( bin.test( 13 ) ) // 7 pin = 8192
		bout.set( 4 );   // 7 pin = 16

	if( bin.test( 12 ) ) // 8 pin = 4096
		bout.set( 3 );   // 8 pin = 8

//----
	if( bin.test( 8 ) )  // 9 pin = 256
		bout.set( 10 );   // 9 pin = 1024

	if( bin.test( 9 ) )  // 10 pin = 512
		bout.set( 0 );   // 10 pin = 1

	if( bin.test( 10 ) ) // 11 pin = 1024
		bout.set( 11 );  // 11 pin = 2048

	if( bin.test( 11 ) ) // 12 pin = 2048
		bout.set( 8 );	 // 12 = 256

	return ( unsigned short )bout.to_ulong();
}

void Klava::printHex( unsigned char *cmd, int size )
{
	printf( "[%d]", size );

	for( int i=0; i<size; i++ )
		printf( "%02X ", cmd[i] );

	printf( "\n" );
}
