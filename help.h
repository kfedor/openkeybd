std::string helpstring="\
<html>\
<head>\
<meta http-equiv=\"content-type\" content=\"text/plain;charset=utf-8\"/>\
<title>Open Keyboard Help</title>\
<link type=\"image/x-icon\" rel=\"icon\" href=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACyElEQVQ4jW2T3UtTARjGdx3edNNVRUgE0UUQUYGBSPQH7DIQb0IiEQokwWJCUhR6E0VNhZZLp3nM6YHwe2vTlZ3jltpZ2fFjKn7tTDfndF9HO78uBpXaA+/dy/O+z/s+j8l0AKqq0ukUcDrt9Pc2MtDXgCg2IQgOFEXhYP8+dDoFJKmd3ehzYBZjx8FezAoskgzXoygdCILj/yStjtekkx+BIPr4SeJjZ5kNVKD676NJV8nKecAov3YlWlutHJqczXiJzVWhzdYy3XuKoK+CpdkPrC70szD5kNXh06SWKtGma8DwIAh2/miWpTayepAVtQ5/3w0ikRkMg32VTGloSgmxuSqy2a+EQj3IsoSp0ymQjr0kIZ9hfiifleWfJLZ1IpE4qVQGw4BUKsPuHhhskwpeZOvTCdLak9wWXd1NwCK73/LRph4wKi9RWHiH4uLH9PfLiKKPoqK7mM0W5hcTGIk3bPqOAQouVxumgb4GkuvNbKjlZOJDSGMLXLhQSnW1DVH0ceXKbdrb3dhsPbjcQdIJGU0pYVt7y0TAjsk92EgyXI+mVkJ6mM+jIc6fv0l1tY2urhEKCsqx2XooK3tGcXEd27ExYuotdtYacgSi2AQskxi/jBGtxT++htlsobS0DlH0MTQUwGy2YDZbeFrbzfL0G/aCZwAVj6cdkyA4SGpW4t6jpCfzSaY0DAMyGZ1MRt/3ifB6jNDwNba8R9iLPs4dUVEUpr4L6Gkp5wOlhM2t8KE3roTX8bvusjRZQTIxQmStF6/Xk/OCIDgACW26huyahaj/EvPBF8ypLuZUF4psZcZznfCPe0TnnwKjf430x8qtVtA9wDhZOQ/Ne5ypT6WoX8qJSefYGTGB4QEkupzW/+dBEOyEQj3o4UeAgh5vRt94lQtWtBZN6zs8+SBkWaLjfTMDg21MBFqYCNhxu9/R8b7lr+Z/8Bsas79cdsS00gAAAABJRU5ErkJggg==\" />\
</head>\
<body style='background: #CCC; font-family: \"Trebuchet MS\";'>\
<b>Справка</b><br /><br />\
<table cellspacing=5 cellpadding=0 border=0>\
<tr><td colspan=3>Скрипты и пример ответов</td></tr>\
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;/lights.cgi</td><td>-</td><td>Получить битовую маску состояния подсветки кнопок</td></tr>\
<tr><td></td><td colspan=2>{<br />\
&nbsp;&nbsp;&nbsp;&nbsp;\"timeNow\": -1197283844, /* Текущее время в миллисекундах */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;\"lights\": 0 /* Битовая маска подсветки кнопок */<br />}\
</td></tr>\
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;/lights.cgi?set=VALUE</td><td>-</td><td>Задать подсветку кнопок, вместо VALUE указать битовую маску кнопок</td></tr>\
<tr><td></td><td colspan=2>{<br />\
&nbsp;&nbsp;&nbsp;&nbsp;\"timeNow\": -1197283844,<br />\
&nbsp;&nbsp;&nbsp;&nbsp;\"lights\": 0<br />}\
</td></tr>\
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;/keys.cgi?set=VALUE</td><td>-</td><td>Получить информацию о состоянии кнопок и задать подсветку кнопок</td></tr>\
<tr><td></td><td colspan=2>{<br />\
&nbsp;&nbsp;&nbsp;&nbsp;\"timeNow\": -1197076310, /* Текущее время в миллисекундах */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;\"lights\": 0,/* Битовая маска подсветки кнопок */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;\"keys\": 6, /* Битовая маска кнопок нажатых/или удерживаемых с момента последнего запроса */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;\"events\": [ /* Массив событий с момента последнего запроса */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{ /* Событие 1 */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\"pressed\": 4, /* Битовая маска нажатых кнопок */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\"released\": 0, /* Битовая маска отпущенных кнопок */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\"time\": -1197078418 /* Время события в миллисекундах */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},<br />\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{ /* Событие 2 */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\"pressed\": 0,<br />\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\"released\": 4,<br />\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\"time\": -1197078322<br />\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br />\
&nbsp;&nbsp;&nbsp;&nbsp;]<br />}\
</td></tr>\
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;/keycodes.cgi</td><td>-</td><td>Получить таблицу кодов кнопок</td></tr>\
<tr><td></td><td colspan=2>{<br />\
&nbsp;&nbsp;&nbsp;&nbsp;\"HELP\": 1, /* HELP/LINE */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;\"LEFT\": 2, /* LEFT/DOUBLE RED/BET ONE */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;\"RIGHT\": 4, /* RIGHT/DOUBLE BLACK/MAX BET */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;\"PLAY1\": 8, /* LINE1/PLAY1 */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;\"PLAY2\": 16, /* LINE3/PLAY2 */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;\"PLAY3\": 32, /* LINE5/PLAY3 */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;\"PLAY4\": 64, /* LINE7/PLAY4 */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;\"PLAY5\": 128, /* LINE9/PLAY5 */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;\"AUTO\": 256, /* AUTO START */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;\"TAKE\": 512, /* START/TAKE */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;\"SELECTOR\": 1024, /* SELECTOR */<br />\
&nbsp;&nbsp;&nbsp;&nbsp;\"COLLECT\": 2048, /* COLLECT */<br />}\
</td></tr>\
</table>\
</body>\
</html>";

std::string keyhelp="\
{\
    \"HELP\": 1, /* HELP/LINE */\
    \"LEFT\": 2, /* LEFT/DOUBLE RED/BET ONE */\
    \"RIGHT\": 4, /* RIGHT/DOUBLE BLACK/MAX BET */\
    \"PLAY1\": 8, /* LINE1/PLAY1 */\
    \"PLAY2\": 16, /* LINE3/PLAY2 */\
    \"PLAY3\": 32, /* LINE5/PLAY3 */\
    \"PLAY4\": 64, /* LINE7/PLAY4 */\
    \"PLAY5\": 128, /* LINE9/PLAY5 */\
    \"AUTO\": 256, /* AUTO START */\
    \"TAKE\": 512, /* START/TAKE */\
    \"SELECTOR\": 1024, /* SELECTOR */\
    \"COLLECT\": 2048 /* COLLECT */\
}";
