#!/bin/sh

VERSION=`sed -n 's/Version: \([0-9\.\-]*\)/\1/p' dist/DEBIAN/control`
PKGNAME="openkeybd_${VERSION}_amd64"

rm dist/usr/local/bin/openkeybd
mkdir -p dist/usr/local/bin
cp -r ../openkeybd dist/usr/local/bin/
rsync -r --exclude=.git dist/ ${PKGNAME}
fakeroot dpkg-deb --build ${PKGNAME}
rm -rf ${PKGNAME}